(function () {
	var express = require('express');
	var bodyParser = require('body-parser');
	var morgan = require('morgan');
	var mysql=require('mysql');
	var Sequalize=require('sequalize')

	var sequalize = new Sequalize('turismo(pro)','root','',{
		host: 'localhost',
		dialect: 'mysql',
        port:'3000',
		pool:{
			max: 20,
			min: 0,
			idle: 10000
		}

	});
	var Usuario = sequalize.define('usuario',{
		id_usuario:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		nombre:{type: Sequalize.STRING, allowNull:false},
		descripcion:{type: Sequalize.STRING, allowNull:false},
		costo:{type: Sequalize.STRING, allowNull:false}
	})
	Usuario.hasMany(contraints = true);
	var Administrador = sequalize.define('administrador',{
		id_administrador:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		nombre:{type: Sequalize.STRING, allowNull:false},
		descripcion:{type: Sequalize.STRING, allowNull:false},
		costo:{type: Sequalize.STRING, allowNull:false}
	})
	Administrador.hasMany(contraints = true);

	//Departamento
	var Departamento = sequalize.define('departamento',{
		id_departamento:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		nombre:{type: Sequalize.STRING, allowNull:false},
		descripcion:{type: Sequalize.STRING, allowNull:false}
	})

	//Lugar Turistico
	var LugarTuristico = sequalize.define('lugar_turistico',{
		id_lugar:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		nombre:{type: Sequalize.STRING, allowNull:false},
		descripcion:{type: Sequalize.STRING, allowNull:false},
		costo:{type:Sequalize.DOUBLE, allowNull:true},
		id_departamento:{type: Sequalize.INTEGER , allowNull:false,foreignKey: true}
	});
	Departamento.hasMany(LugarTuristico,{foreignKey: 'id_departamento',constraints:true });
	LugarTuristico.belongsTo(Departamento,{foreignKey:'id_departamento', constraints: true});

	//Hotel
	var Hotel = sequalize.define('hotel',{
		id_hotel:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		nombre:{type: Sequalize.STRING, allowNull:false},
		descripcion:{type: Sequalize.STRING, allowNull:false},
		categoria:{type: Sequalize.INTEGER, allowNull:false},
		costo:{type:Sequalize.DOUBLE, allowNull:false},
		direccion:{type:Sequalize.STRING, allowNull:false},
		id_departamento:{type: Sequalize.INTEGER , allowNull:false,foreignKey: true}
	});
	Departamento.hasMany(Hotel,{foreignKey: 'id_departamento',constraints:true });
	Hotel.belongsTo(Departamento,{foreignKey:'id_departamento', constraints: true});

	//Comentario
	var Comentario = sequalize.define('comentario',{
		id_comentario:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		contenido:{type: Sequalize.STRING, allowNull:false},
		id_usuario:{type: Sequalize.INTEGER, allowNull:false,foreignKey: true},
		id_hotel:{type: Sequalize.INTEGER, allowNull:true,foreignKey: true},
		id_lugar:{type: Sequalize.INTEGER, allowNull:true,foreignKey: true}
	});
	LugarTuristico.hasMany(Comentario,{foreignKey: 'id_lugar',constraints:true });
	Comentario.belongsTo(LugarTuristico,{foreignKey:'id_lugar', constraints: true});
	Hotel.hasMany(Comentario,{foreignKey: 'id_hotel',constraints:true });
	Comentario.belongsTo(Hotel,{foreignKey:'id_hotel', constraints: true});
	Usuario.hasMany(Comentario,{foreignKey: 'id_usuario',constraints:true });
	Comentario.belongsTo(Usuario,{foreignKey:'id_usuario', constraints: true});


	//Reporte
	var Reporte = sequalize.define('reporte',{
		id_reporte:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		informe:{type: Sequalize.STRING, allowNull:false},
		id_administrador:{type: Sequalize.INTEGER, allowNull:false,foreignKey: true},
		id_departamento:{type: Sequalize.INTEGER, allowNull:false,foreignKey:true}
	});
	Departamento.hasMany(Reporte,{foreignKey: 'id_departamento',constraints:true });
	Reporte.belongsTo(Departamento,{foreignKey:'id_departamento', constraints: true});
	Administrador.hasMany(Reporte,{foreignKey: 'id_administrador',constraints:true });
	Reporte.belongsTo(Administrador,{foreignKey:'id_administrador', constraints: true});

	//Interes
	var Interes = sequalize.define('interes',{
		id_interes:{type: Sequalize.INTEGER, primaryKey:true,autoIncrement: true},
		interesado:{type: Sequalize.BOOLEAN, allowNull:false},
		id_usuario:{type: Sequalize.INTEGER, allowNull:false,foreignKey: true},
		id_departamento:{type: Sequalize.INTEGER, allowNull:false, foreignKey : true}
	});
	Departamento.hasMany(Interes,{foreignKey: 'id_departamento',constraints:true });
	Interes.belongsTo(Departamento,{foreignKey:'id_departamento', constraints: true});
	Usuario.hasMany(Interes,{foreignKey: 'id_usuario',constraints:true });
	Interes.belongsTo(Usuario,{foreignKey:'id_usuario', constraints: true});

	sequalize.sync({force:true});
	var puerto=3000;
	var conf=require('./config');
	var app=express();
	app.use(bodyparser.json());
	app.use('/api/v1',require('./rutas')(app));
	app.set('usuario', Departamento);
	app.listen(puerto,function () {
		console.log("Servidor iniciado en el puerto: " +puerto);
		console.log("Debug del server: ");
	});
})