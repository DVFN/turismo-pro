/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Reporte = app.get('reporte');
            Reporte.create({
                informe : req.body.informe
            }).then(function(reporte){
                res.json(reporte);
            });
        },
        list:function(req,res){
            var Reporte = app.get('informe');
            Reporte.findAll().then(function (reporte){
                res.json(reporte);
            });
        },
        edit:function(req,res){
            var Reporte = app.get('reporte');
            Reporte.find(req.body.id_reporte).then(function(reporte){
                if(reporte){
                    reporte.updateAtrributes({
                        informe: req.body.informe
                    }).then(function (reporte){
                        res.json(reporte);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el reporte"});

                }

            });
        },
        delete:function (req,res) {
            var Reporte = app.get('reporte');
            Reporte.destroy({
                where:{
                    id_reporte: req.body.id_reporte
                }
            }).then(function (reporte) {

                res.json(reporte);

            })
        },
        porid:function(req,res){
            var Reporte = app.get('reporte');
            Reporte.find(req.body.id_reporte).then(function(reporte){
                if(reporte){
                    res.json(reporte);
                }else{
                    res.status(404).({message : "No se encuentra el reporte"});
                }
            });
        }
    }
}
