/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var LugarTuristico = app.get('lugar');
            LugarTuristico.create({
                nombre : req.body.nombre,
                descripcion: req.body.descripcion,
                costo: req.body.costo
            }).then(function(lugar){
                res.json(lugar);
            });
        },
        list:function(req,res){
            var LugarTuristico = app.get('lugar');
            LugarTuristico.findAll().then(function (lugar){
                res.json(lugar);
            });
        },
        edit:function(req,res){
            var LugarTuristico = app.get('lugar');
            LugarTuristico.find(req.body.id_lugar).then(function(lugar){
                if(lugar){
                    lugar.updateAtrributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcion,
                        costo: req.body.costo
                    }).then(function (lugar){
                        res.json(lugar);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el lugar turistico"});

                }

            });
        },
        delete:function (req,res) {
            var LugarTuristico = app.get('lugar');
            LugarTuristico.destroy({
                where:{
                    id_lugar: req.body.id_lugar
                }
            }).then(function (lugar) {

                res.json(lugar);

            })
        },
        porid:function(req,res){
            var LugarTuristico = app.get('lugar');
            LugarTuristico.find(req.body.id_lugar).then(function(lugar){
                if(lugar){
                    res.json(lugar);
                }else{
                    res.status(404).({message : "No se encuentra el lugar turistico"});
                }
            });
        }
    }
}