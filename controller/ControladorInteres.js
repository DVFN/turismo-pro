/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Interes = app.get('interes');
            Interes.create({
                interesado : req.body.interesado
            }).then(function(interes){
                res.json(interes);
            });
        },
        list:function(req,res){
            var Interes = app.get('interes');
            Interes.findAll().then(function (interes){
                res.json(interes);
            });
        },
        edit:function(req,res){
            var Interes = app.get('interes');
            Interes.find(req.body.id_interes).then(function(interes){
                if(interes){
                    interes.updateAtrributes({
                        interesado : req.body.interesado
                    }).then(function (interes){
                        res.json(interes);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el interesado"});

                }

            });
        },
        delete:function (req,res) {
            var Interes = app.get('interes');
            Interes.destroy({
                where:{
                    id_interes: req.body.id_interes
                }
            }).then(function (interes) {

                res.json(interes);

            })
        },
        porid:function(req,res){
            var Interes = app.get('interes');
            Interes.find(req.body.id_interes).then(function(interes){
                if(interes){
                    res.json(interes);
                }else{
                    res.status(404).({message : "No se encuentra el interesado"});
                }
            });
        }
    }
}
