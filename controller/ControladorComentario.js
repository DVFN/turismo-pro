/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Comentario = app.get('comentario');
            Comentario.create({
                comentario : req.body.comentario
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        list:function(req,res){
            var Comentario = app.get('comentario');
            Comentario.findAll().then(function (comentario){
                res.json(comentario);
            });
        },
        edit:function(req,res){
            var Comentario = app.get('hotel');
            Comentario.find(req.body.id_comentario).then(function(comentario){
                if(comentario){
                    comentario.updateAtrributes({
                        comentario : req.body.comentario
                    }).then(function (comentario){
                        res.json(comentario);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el comentario"});

                }

            });
        },
        delete:function (req,res) {
            var Comentario = app.get('comentario');
            Comentario.destroy({
                where:{
                    id_comentario: req.body.id_comentario
                }
            }).then(function (comentario) {

                res.json(comentario);

            })
        },
        porid:function(req,res){
            var Comentario = app.get('comentario');
            Comentario.find(req.body.id_comentario).then(function(comentario){
                if(comentario){
                    res.json(comentario);
                }else{
                    res.status(404).({message : "No se encuentra el comentario"});
                }
            });
        }
    }
}