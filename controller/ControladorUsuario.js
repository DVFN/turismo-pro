module.exports = function(app){
    return{
        add:function(req , res){
            var Usuario = app.get('usuario');
            Usuario.create({
                nombre : req.body.nombre,
                descripcion: req.body.descripcion,
                costo: req.body.costo
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        list:function(req,res){
            var Usuario = app.get('usuario');
            Usuario.findAll().then(function (usuarios){
                res.json(usuarios);
            });
        },
        edit:function(req,res){
            var Usuario = app.get("usuario");
            Usuario.find(req.body.id_usuario).then(function(usuario){
                if(usuario){
                    usuario.updateAtrributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcion,
                        costo: req.body.costo
                    }).then(function (usuario){
                        res.json(usuario);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el usuario"});

                }

            });
        },
        delete:function (req,res) {
            var Usuario = app.get('usuario');
            Usuario.destroy({
                where:{
                    id_usuario: req.body.id_usuario
                }
            }).then(function (usuario) {

                res.json(usuario);

            })
        },
        porid:function(req,res){
            var Usuario = app.get('usuario');
            Usuario.find(req.body.id_usuario).then(function(usuario){
                if(usuario){
                    res.json(usuario);
                }else{
                    res.status(404).({message : "No se encuentra el usuario"});
                }
            });
        }
    }
}