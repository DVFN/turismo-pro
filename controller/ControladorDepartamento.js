/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Departamento = app.get('departamento');
            Departamento.create({
                nombre : req.body.nombre,
                descripcion: req.body.descripcion
            }).then(function(departamento){
                res.json(departamento);
            });
        },
        list:function(req,res){
            var Departamento = app.get('departamento');
            Departamento.findAll().then(function (departamentos){
                res.json(departamentos);
            });
        },
        edit:function(req,res){
            var Departamento = app.get('departamento');
            Departamento.find(req.body.id_departamento).then(function(departamento){
                if(departamento){
                    departamento.updateAtrributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcion
                    }).then(function (departamento){
                        res.json(departamento);
                    });
                }else{
                    res.status(404).({message: "no se encuentra departamento"});

                }

            });
        },
        delete:function (req,res) {
         var Departamento = app.get('departamento');
            Departamento.destroy({
               where:{
                   id_departamentos: req.body.id_departamento
               } 
            }).then(function (departamento) {

                res.json(departamento);
                
            })
        },
        porid:function(req,res){
            var Departamento = app.get('departamento');
            Departamento.find(req.body.id_departamento).then(function(departamento){
              if(departamento){
                  res.json(departamento);
              }else{
                  res.status(404).({message : "No se encuentra el departamento"});
              }
            });
        }
    }
}