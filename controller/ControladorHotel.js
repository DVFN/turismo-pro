/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Hotel = app.get('hotel');
            Hotel.create({
                nombre : req.body.nombre,
                descripcion: req.body.descripcion,
                categoria: req.body.categoria,
                costo: req.body.costo,
                direccion: req.body.direccion
            }).then(function(hotel){
                res.json(hotel);
            });
        },
        list:function(req,res){
            var Hotel = app.get('hotel');
            Hotel.findAll().then(function (hotel){
                res.json(hotel);
            });
        },
        edit:function(req,res){
            var Hotel = app.get('hotel');
            Hotel.find(req.body.id_hotel).then(function(hotel){
                if(hotel){
                    hotel.updateAtrributes({
                        nombre : req.body.nombre,
                        descripcion: req.body.descripcion,
                        categoria: req.body.categoria,
                        costo: req.body.costo,
                        direccion: req.body.direccion
                    }).then(function (hotel){
                        res.json(hotel);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el hotel"});

                }

            });
        },
        delete:function (req,res) {
            var Hotel = app.get('hotel');
            Hotel.destroy({
                where:{
                    id_hotel: req.body.id_hotel
                }
            }).then(function (hotel) {

                res.json(hotel);

            })
        },
        porid:function(req,res){
            var Hotel = app.get('hotel');
            Hotel.find(req.body.id_usuario).then(function(hotel){
                if(hotel){
                    res.json(hotel);
                }else{
                    res.status(404).({message : "No se encuentra el hotel"});
                }
            });
        }
    }
}