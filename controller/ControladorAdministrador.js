/**
 * Created by devin on 6/05/2016.
 */
module.exports = function(app){
    return{
        add:function(req , res){
            var Administrador = app.get('administrador');
            Administrador.create({
                nombre : req.body.nombre,
                descripcion: req.body.descripcion,
                costo: req.body.costo
            }).then(function(administrador){
                res.json(administrador);
            });
        },
        list:function(req,res){
            var Administrador = app.get('administrador');
            Administrador.findAll().then(function (administrador){
                res.json(administrador);
            });
        },
        edit:function(req,res){
            var Administrador = app.get('administrador');
            Administrador.find(req.body.id_administrador).then(function(administrador){
                if(administrador){
                    administrador.updateAtrributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcion,
                        costo: req.body.costo
                    }).then(function (administrador){
                        res.json(administrador);
                    });
                }else{
                    res.status(404).({message: "no se encuentra el administrador"});

                }

            });
        },
        delete:function (req,res) {
            var Administrador = app.get('administrador');
            Administrador.destroy({
                where:{
                    id_administrador: req.body.id_administrador
                }
            }).then(function (administrador) {

                res.json(administrador);

            })
        },
        porid:function(req,res){
            var Administrador = app.get('administrador');
            Administrador.find(req.body.id_administrador).then(function(administrador){
                if(administrador){
                    res.json(administrador);
                }else{
                    res.status(404).({message : "No se encuentra el administrador"});
                }
            });
        }
    }
}
