
var ruta = require('express').Router();
module.exports =(function(app){
	var usuario = require("../controller/ControladorUsuario")(app);
	var administrador = require ("../controller/ControladorAdministrador")(app);
	var departamento = require("../controller/ControladorDepartamento")(app);
	var lugarTuristico = require("../controller/ControladorLugarTuristico")(app);
	var hotel = require("../controller/ControladorHotel")(app);
	var comentario = require("../controller/ControladorComentario")(app);
	var reporte = require("../controller/ControladorReporte")(app);
	var interes = require("../controller/ControladorInteres")(app);

	//Rutas para usuario
	ruta.get('/usuario', usuario.list);
	ruta.post('/usuario',usuario.add);
	ruta.put('/usuario', usuario.edit);
	ruta.delete('/usuario',usuario.delete);
	ruta.get('/usuario/:id', usuario.porid);

	//Rutas para administrador
	ruta.get('/administrador', administrador.list);
	ruta.post('/administrador',administrador.add);
	ruta.put('/administrador', administrador.edit);
	ruta.delete('/administrador',administrador.delete);
	ruta.get('/administrador/:id', administrador.porid);

	//Rutas para departamento
	ruta.get('/departamento', departamento.list);
	ruta.post('/departamento',departamento.add);
	ruta.put('/departamento', departamento.edit);
	ruta.delete('/departamento',departamento.delete);
	ruta.get('/departamento/:id', departamento.porid);

	//Rutas para lugarTuristico
	ruta.get('/lugarTuristico', lugarTuristico.list);
	ruta.post('/lugarTuristico',lugarTuristico.add);
	ruta.put('/lugarTuristico', lugarTuristico.edit);
	ruta.delete('/lugarTuristico',lugarTuristico.delete);
	ruta.get('/lugarTuristico/:id', lugarTuristico.porid);

	//Rutas para hotel
	ruta.get('/hotel', hotel.list);
	ruta.post('/hotel',hotel.add);
	ruta.put('/hotel', hotel.edit);
	ruta.delete('/hotel',hotel.delete);
	ruta.get('/hotel/:id', hotel.porid);

	//Rutas para comentario
	ruta.get('/comentario', comentario.list);
	ruta.post('/comentario',comentario.add);
	ruta.put('/comentario', comentario.edit);
	ruta.delete('/comentario',comentario.delete);
	ruta.get('/comentario/:id', comentario.porid);

	//Rutas para reporte
	ruta.get('/reporte', reporte.list);
	ruta.post('/reporte',reporte.add);
	ruta.put('/reporte', reporte.edit);
	ruta.delete('/reporte',reporte.delete);
	ruta.get('/reporte/:id', reporte.porid);

	//Rutas para interes
	ruta.get('/interes', interes.list);
	ruta.post('/interes',interes.add);
	ruta.put('/interes', interes.edit);
	ruta.delete('/interes',interes.delete);
	ruta.get('/interes/:id', interes.porid);
});